<?php


$code = $GET['code'];

if ($code == "") {
    header('Location: http://localhost:7071');
    exit;
}


$CLIENT_ID = "7980939365316925551e";
$CLIENT_SECRET = "d2e923bad7b08ce3f44a86f80d730c1c3a13e7f6";
$URL = "https://github.com/login/oauth/access_token";

$postParams = [
    'client_id' => $CLIENT_ID,
    'client_secret' =>$CLIENT_SECRET,
    'code' => $code
];

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $URL);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postParams);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
$response = curl_exec($ch);
curl_close($ch);
$data = json_decode($response);

if ($data->access_token != "") {
    $_SESSION['my_access_token_accessToken'] = $data['access_token'];
    header('Location: http://localhost:7071/login.html');
    exit;   
}

?>