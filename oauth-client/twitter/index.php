<?php

require 'config.php';
require 'twitter-api-login/autoload.php';

//HELPER TWITTER trouvé sur github
use Abraham\TwitterOAuth\TwitterOAuth;
//

if (isset($_SESSION['twitter_acces_token']) && $_SESSION['twitter-access_token']) {
    $isLoggedIn = true;
} elseif (isset($_GET['oauth_verifier']) && isset($_GET['oauth_token']) && isset($_SESSION['oauth_token']) && $_GET['oauth_token'] == $_SESSION['oauth_token']) {
    $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

    $access_token = $connection->oauth("oauth/access_token", array("oauth_verifier" => $_GET['oauth_verifier']));

    $_SESSION['twitter_access_token'] = $access_token;

    $isLoggedIn = true;
} else {
    $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);

    $request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => OAUTH_CALLBACK));

    $_SESSION['oauth_token'] = $request_token['oauth_token'];
    $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

    $isLoggedIn = false;
}

if ($isLoggedIn) {
    $oauthToken = $_SESSION['twitter_access_token']['oauth_token'];
    $oauthTokenSecret = $_SESSION['twitter_access_token']['oauth_token_secret'];

    $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $oauthToken, $oauthTokenSecret);

    $user = $connection->get("account/verify_credentials", ['include_email' => 'true']);

} else {
    $url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));
    ?>
    <a href="<?php echo $url; ?>">Login With Twitter</a> 
<?php
}
